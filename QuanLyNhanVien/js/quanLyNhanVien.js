//DANH SACH SINH VIEN
var danhSachNhanVien = [];

// LAY DU LIEU TU LOCAL
var dataLocal = localStorage.getItem("DANH_SACH_NHAN_VIEN");
if (dataLocal != null) {
  danhSachNhanVien = JSON.parse(dataLocal).map(function (item) {
    return new NhanVien(
      item.taiKhoan,
      item.tenNhanVien,
      item.email,
      item.password,
      item.ngayLamViec,
      item.luongCoBan,
      item.chucVu,
      item.gioLam
    );
  });
  renderNhanVien(danhSachNhanVien);
}

//THEM NHAN VIEN
function themNhanVien() {
  var nhanVien = layValueTuForm();
  var isVali = valiTaiKhoan();
  isVali = isVali & valiEmail();
  isVali = isVali & valiPassword();
  isVali = isVali & valiUser();
  isVali = isVali & valiLuongCoBan();
  isVali = isVali & valiChucVu();
  isVali = isVali & valiGioLam();
  isVali = isVali & valiDate();
  if (isVali == true) {
    danhSachNhanVien.push(nhanVien);
    renderNhanVien(danhSachNhanVien);
    localDSSV();
  }
  return nhanVien;
}

// RENDER NHAN VIEN
function renderNhanVien(danhSachNhanVien) {
  var HTML = "";
  for (var i = 0; i < danhSachNhanVien.length; i++) {
    var item = danhSachNhanVien[i];
    HTML += `<tr>
    <td>${item.taiKhoan}</td>
    <td>${item.tenNhanVien}</td>
    <td>${item.email}</td>
    <td>${item.ngayLamViec}</td>
    <td>${item.chucVu}</td>
    <td>${item.tongLuong()}</td>
    <td>${item.xepLoai()}</td>
    <td><button class="btn btn-danger" onclick="xoaNhanVien(${
      item.taiKhoan
    })">XOÁ</button>
    <button class="btn btn-success" id="suaNhanVien" onclick="suaNhanVien(${
      item.taiKhoan
    })" data-toggle="modal" data-target="#myModal">SỬA</button>
    </tr>`;
    document.getElementById("tableDanhSach").innerHTML = HTML;
  }
}

// LUU XUONG LOCAL STORAGE
function localDSSV() {
  var dataLocal = JSON.stringify(danhSachNhanVien);
  localStorage.setItem("DANH_SACH_NHAN_VIEN", dataLocal);
}

// XOA NHAN VIEN
function xoaNhanVien(id) {
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  console.log(viTri);
  if (viTri != -1) {
    danhSachNhanVien.splice(viTri, 1);
    renderNhanVien(danhSachNhanVien);
    localDSSV();
  }
}

// SUA NHAN VIEN
function suaNhanVien(id) {
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == id;
  });
  console.log(viTri);
  document.getElementById("tknv").value = danhSachNhanVien[viTri].taiKhoan;
  document.getElementById("name").value = danhSachNhanVien[viTri].tenNhanVien;
  document.getElementById("email").value = danhSachNhanVien[viTri].email;
  document.getElementById("password").value = danhSachNhanVien;
  document.getElementById("datepicker").value =
    danhSachNhanVien[viTri].ngayLamViec;
  document.getElementById("luongCB").value = danhSachNhanVien[viTri].luongCoBan;
  document.getElementById("chucvu").value = danhSachNhanVien[viTri].chucVu;
  document.getElementById("gioLam").value = danhSachNhanVien[viTri].gioLam;
}

// DONG
document.getElementById("btnDong").onclick = function () {
  resetForm();
};

// CAP NHAT SINH VIEN
document.getElementById("btnCapNhat").onclick = function () {
  var nhanVien = layValueTuForm();
  var viTri = danhSachNhanVien.findIndex(function (item) {
    return item.taiKhoan == nhanVien.taiKhoan;
  });
  var isVali = valiTaiKhoan();
  isVali = isVali & valiEmail();
  isVali = isVali & valiPassword();
  isVali = isVali & valiUser();
  isVali = isVali & valiLuongCoBan();
  isVali = isVali & valiChucVu();
  isVali = isVali & valiGioLam();
  if (isVali == true) {
    danhSachNhanVien[viTri] = nhanVien;
    // NGUY HIEM NEU NGUOC LAI
    renderNhanVien(danhSachNhanVien);
    localDSSV();
  }
};

// LAY THONG TIN NHAP TU FORM
function layValueTuForm() {
  var taiKhoan = document.getElementById("tknv").value;
  var tenNhanVien = document.getElementById("name").value;
  var email = document.getElementById("email").value;
  var password = document.getElementById("password").value;
  var ngayLamViec = document.getElementById("datepicker").value;
  var luongCoBan = document.getElementById("luongCB").value;
  var chucVu = document.getElementById("chucvu").value;
  var gioLam = document.getElementById("gioLam").value;
  return new NhanVien(
    taiKhoan,
    tenNhanVien,
    email,
    password,
    ngayLamViec,
    luongCoBan,
    chucVu,
    gioLam
  );
}

// VALIDATION FORM TAI KHOAN
function valiTaiKhoan() {
  var taiKhoan = document.getElementById("tknv").value;
  document.getElementById("tbTKNV").style.display = "block";
  if (taiKhoan.length <= 6) {
    if (taiKhoan === "" || taiKhoan === null) {
      document.getElementById("tbTKNV").innerHTML = "sai tai khoan";
      return false;
    }
    document.getElementById("tbTKNV").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbTKNV").innerHTML = "sai tai khoan";
    return false;
  }
}

// VALIDATION EMAIL
function valiEmail() {
  document.getElementById("tbEmail").style.display = "block";
  var email = document.getElementById("email").value;
  const re =
    /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  if (re.test(email)) {
    document.getElementById("tbEmail").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbEmail").innerHTML = "sai";
    return false;
  }
}

//VALIDATION PASSWORD
function valiPassword() {
  document.getElementById("tbMatKhau").style.display = "block";
  var password = document.getElementById("password").value;
  var re = /^(?=.*[0-9])(?=.*[!@#$%^&*])(?=.*[A-Z])[a-zA-Z0-9!@#$%^&*]{6,10}$/;
  if (re.test(password)) {
    document.getElementById("tbMatKhau").innerHTML = "";
    return true;
  }
  document.getElementById("tbMatKhau").innerHTML = "sai";
  return false;
}

//VALIDATION USERNAME
function valiUser() {
  document.getElementById("tbTen").style.display = "block";
  var name = document.getElementById("name").value;
  var re = /^[a-zA-Z\s]*$/;
  if (name == "") {
    document.getElementById("tbTen").innerHTML = "sai";
    return false;
  }
  if (re.test(name)) {
    document.getElementById("tbTen").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbTen").innerHTML = "sai";
    return false;
  }
}

//VALIDATION LUONG CO BAN
function valiLuongCoBan() {
  document.getElementById("tbLuongCB").style.display = "block";
  var luongCoBan = document.getElementById("luongCB").value;
  if (luongCoBan >= 100000 && luongCoBan <= 20000000) {
    document.getElementById("tbLuongCB").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbLuongCB").innerHTML = "sai";
    return false;
  }
}

//VALIDATE NGAY

//VALIDATION CHUC VU
function valiChucVu() {
  document.getElementById("tbChucVu").style.display = "block";
  var chucVu = document.getElementById("chucvu").value;
  if (chucVu == "Sếp" || chucVu == "Trưởng Phòng" || chucVu == "Nhân Viên") {
    document.getElementById("tbChucVu").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbChucVu").innerHTML = "sai";
    return false;
  }
}

// VALIDATION GIO LAM,
function valiGioLam() {
  document.getElementById("tbGiolam").style.display = "block";
  var gioLam = document.getElementById("gioLam").value;
  if (gioLam >= 80 && gioLam <= 200) {
    document.getElementById("tbGiolam").innerHTML = "";
    return true;
  } else {
    document.getElementById("tbGiolam").innerHTML = "sai";
    return false;
  }
}

// FUNCTION SEACH
document.getElementById("btnTimNV").onclick = function search() {
  var search = document.getElementById("searchName").value.trim();
  if (search === "Nhân viên xuất sắc".toLowerCase()) {
    let nhanVienSame = danhSachNhanVien.filter(function (item) {
      return item.gioLam >= 192;
    });
    renderNhanVien(nhanVienSame);
  } else if (search == "Nhân viên giỏi".toLowerCase()) {
    let nhanVienSame = danhSachNhanVien.filter(function (item) {
      return item.gioLam >= 176 && item.gioLam < 192;
    });
    renderNhanVien(nhanVienSame);
  } else if (search == "Nhân viên khá".toLowerCase()) {
    let nhanVienSame = danhSachNhanVien.filter(function (item) {
      return item.gioLam >= 160 && item.gioLam < 176;
    });
    renderNhanVien(nhanVienSame);
  } else {
    let nhanVienSame = danhSachNhanVien.filter(function (item) {
      return item.gioLam < 160;
    });
    renderNhanVien(nhanVienSame);
  }
};
