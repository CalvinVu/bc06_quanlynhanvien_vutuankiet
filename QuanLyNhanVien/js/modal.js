// Show/hide password onClick of button using Javascript only

// https://stackoverflow.com/questions/31224651/show-hide-password-onclick-of-button-using-javascript-only

// function show() {
//   var p = document.getElementById("pwd");
//   p.setAttribute("type", "text");
// }

// function hide() {
//   var p = document.getElementById("pwd");
//   p.setAttribute("type", "password");
// }

// var pwShown = 0;

// document.getElementById("eye").addEventListener(
//   "click",
//   function () {
//     if (pwShown == 0) {
//       pwShown = 1;
//       show();
//     } else {
//       pwShown = 0;
//       hide();
//     }
//   },
//   false
// );

//   font-family: 'Vibur', cursive;
//   font-family: 'Abel', sans-serif;
// font-family: 'Pacifico', cursive;
// font-family: 'Dancing Script', cursive;
// font-family: 'Alegreya', serif;
// font-family: 'Abril Fatface', cursive;
// font-family: 'Playball', cursive;
// font-family: 'Unica One', cursive;
// font-family: 'Oleo Script', cursive;
// font-family: 'Share', cursive;
// font-family: 'Overlock', cursive;
// font-family: 'Arima Madurai', cursive;
// font-family: 'Playfair Display', serif;
// font-family: 'Merriweather', serif;
// font-family: 'PT Serif', serif;
// font-family: 'Dosis', sans-serif;

function NhanVien(
  taiKhoan,
  tenNhanVien,
  email,
  password,
  ngayLamViec,
  luongCoBan,
  chucVu,
  gioLam
) {
  this.taiKhoan = taiKhoan;
  this.tenNhanVien = tenNhanVien;
  this.email = email;
  this.password = password;
  this.ngayLamViec = ngayLamViec;
  this.luongCoBan = luongCoBan;
  this.chucVu = chucVu;
  this.gioLam = gioLam;
  this.tongLuong = function () {
    const VND = new Intl.NumberFormat("vi-VN", {
      style: "currency",
      currency: "VND",
    });
    if (this.chucVu == "Sếp") {
      return VND.format(this.luongCoBan * 3);
    } else if (this.chucVu == "Trưởng Phòng") {
      return VND.format(this.luongCoBan * 2);
    } else {
      return VND.format(this.luongCoBan * 1);
    }
  };
  this.xepLoai = function () {
    if (this.gioLam >= 192) {
      return "Nhân viên xuất sắc";
    } else if (this.gioLam >= 176) {
      return "Nhân viên giỏi";
    } else if (this.gioLam >= 160) {
      return "Nhân viên khá";
    } else {
      return "Nhân viên trung bình";
    }
  };
}
